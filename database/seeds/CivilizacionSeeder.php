<?php

use App\Civilizacion;
use Illuminate\Database\Seeder;

class CivilizacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $civilizaciones = [
            ['nombre' => 'Chino', 'cantidad_piqueros' => 2, 'cantidad_arqueros' => 25, 'cantidad_caballeros' => 2],
            ['nombre' => 'Ingleses', 'cantidad_piqueros' => 10, 'cantidad_arqueros' => 10, 'cantidad_caballeros' => 10],
            ['nombre' => 'Bizantinos', 'cantidad_piqueros' => 5, 'cantidad_arqueros' => 8, 'cantidad_caballeros' => 15],
        ];

        foreach ($civilizaciones as $civilizacion) {
            $civilizacionNueva = Civilizacion::create(['nombre' => $civilizacion['nombre']]);
            $civilizacionNueva->tiposUnidades()->sync([
                3 => $civilizacion['cantidad_piquero'],
                2 => $civilizacion['cantidad_arqueros'],
                1 => $civilizacion['cantidad_caballeros'],
            ]);
        }

    }
}
