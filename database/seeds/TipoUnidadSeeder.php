<?php

use App\TipoUnidad;
use Illuminate\Database\Seeder;

class TipoUnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        como no pidieron instanciar nada en la DB
        dejo declarado el array que seedearia usando al factory que esta vacia.
        algo asi :
        //*/
        $tiposUnidades =
            [
            [
                'nombre' => 'Caballero',
                'transformacion_id' => null,
                'fuerza_inicial' => 5,
                'fuerza_por_entrenamiento' => 10,
                'costo_entrenamiento' => 30,
                'costo_transformacion' => null,
            ],

            [
                'nombre' => 'Arquero',
                'transformacion_id' => 1,
                'fuerza_inicial' => 5,
                'fuerza_por_entrenamiento' => 7,
                'costo_entrenamiento' => 20,
                'costo_transformacion' => 40,
            ],
            [
                'nombre' => 'Piquero',
                'transformacion_id' => 2,
                'fuerza_inicial' => 5,
                'fuerza_por_entrenamiento' => 3,
                'costo_entrenamiento' => 10,
                'costo_transformacion' => 30,
            ],

        ];

        foreach ($tiposUnidades as $tipoUnidad) {
            factory(TipoUnidad::class, 1)->create([
                'nombre' => $tipoUnidad['nombre'],
                'transformacion_id' => $tipoUnidad['transformacion_id'],
                'fuerza_inicial' => $tipoUnidad['fuerza_inicial'],
                'fuerza_por_entrenamiento' => $tipoUnidad['fuerza_por_entrenamiento'],
                'costo_entrenamiento' => $tipoUnidad['costo_entrenamiento'],
            ]);
        }

    }
}
