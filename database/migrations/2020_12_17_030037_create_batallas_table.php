<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatallasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batallas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('atacante_id')->constrained('ejercitos');
            $table->foreignId('defensor_id')->constrained('ejercitos');
            $table->foreignId('vencedor_id')->nullable()->constrained('ejercitos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batallas');
    }
}
