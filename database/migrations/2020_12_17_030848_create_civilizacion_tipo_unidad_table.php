<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCivilizacionTipoUnidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('civilizacion_tipo_unidad', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->foreignId('civilizacion_id')->constrained('civilizaciones');
            $table->foreignId('tipo_unidad_id')->constrained('tipos_unidades');
            $table->integer('cantidad_inicial');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('civilizacion_tipo_unidad');
    }
}
