<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_unidades', function (Blueprint $table) {
            $table->id();

            $table->foreignId('transformacion_id')->nullable()->constrained('tipos_unidades');

            $table->string('nombre');
            $table->integer('fuerza_inicial');
            $table->integer('costo_entrenamiento');
            $table->integer('costo_transformacion')->nullable();
            $table->integer('fuerza_por_entrenamiento');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_unidades');
    }
}
