<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ejercito extends Model
{
    protected $table = 'ejercitos';
    protected $fillable = [
        'nombre', 'monedas_oro', 'civilizacion_id',
    ];

    public function civilizacion()
    {
        return $this->belongsTo('App\Civilizacion');
    }

    public function unidades()
    {
        return $this->hasMany('App\Unidad');
    }

    public function scopeUnidadesMasPoderosas($query, $cantidad = 2)
    {
        return $this->unidades()->orderby('puntos', 'desc')->take($cantidad);
    }

    public function batallas()
    {
        return $this->hasMany('App\Batallas', 'atacante_id');
    }

    public function atacar(Ejercito $enemigo)
    {
        if (empty($enemigo)) {
            return false;
        }

        $poderAtacante = $this->unidades()->sum('puntos');
        $poderDefensor = $enemigo->unidades()->sum('puntos');
        /*
        no me copa mucho usar los switch porque suele violar el principio abierto-cerrado de o la O de SOLID
        PERO, como en este caso solamente existen 3 posibilidades y eso no puede incrementarse los casos de uso
        a futuro, entonces no es TAN grave.
        //*/
        DB::transaction(function () use ($poderAtacante, $poderDefensor, $enemigo) {

            switch (true) {
                case ($poderAtacante > $poderDefensor):

                    $this->monedas_oro += 100;
                    $this->save();

                    $unidadesMasPoderosas = $enemigo->unidadesMasPoderosas();
                    $enemigo->unidades()->whereIn('id', $unidadesMasPoderosas->pluck('id'))->delete();

                    $this->registrarResultadoBatalla($this, $enemigo, $this);

                case ($poderAtacante < $poderDefensor):
                    $enemigo->monedas_oro += 100;
                    $enemigo->save();

                    $unidadesMasPoderosas = $this->unidadesMasPoderosas();
                    $this->unidades()->whereIn('id', $unidadesMasPoderosas->pluck('id'))->delete();

                    $this->registrarResultadoBatalla($enemigo, $this, $enemigo);

                    break;
                default:
                    //en caso de empate cada ejercito pierde su unidad mas poderosa.

                    $unidadesMasPoderosas = $enemigo->unidadesMasPoderosas(1);
                    $enemigo->unidades()->whereIn('id', $unidadesMasPoderosas->pluck('id'))->delete();

                    $unidadesMasPoderosas = $this->unidadesMasPoderosas(1);
                    $this->unidades()->whereIn('id', $unidadesMasPoderosas->pluck('id'))->delete();

                    $this->registrarResultadoBatalla($this, $enemigo);

                    break;
            }
        });
    }

    public function registrarResultadoBatalla(Ejercito $atacante, Ejercito $defensor, Ejercito $victorioso = null)
    {
        return Batalla::create([
            'atacante_id' => $atacante->id,
            'defensor_id' => $defensor->id,
            'victorioso_id' => !empty($victorioso->id) ? $victorioso->id : null,
        ]);
    }
}
