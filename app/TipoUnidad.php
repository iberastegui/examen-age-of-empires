<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUnidad extends Model
{
    protected $table = 'tipos_unidades';
    protected $fillable = ['transformacion_id',
        'nombre', 'fuerza_inicial', 'costo_entrenamiento', 'costo_transformacion
        ', 'fuerza_por_entrenamiento'];
}
