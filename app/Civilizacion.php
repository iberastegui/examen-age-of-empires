<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Civilizacion extends Model
{
    protected $fillable = [
        'nombre',
    ];

    public function tiposUnidades()
    {
        return $this->belongsToMany('App\TipoUnidad')->withPivot(['cantidad']);
    }
}
