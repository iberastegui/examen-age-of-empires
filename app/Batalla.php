<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batalla extends Model
{
    protected $table = 'batallas';
    protected $fillable = ['atacante_id', 'defensor_id', 'vencedor_id'];

    public function atacante()
    {
        return $this->belongsTo('App\Ejercito');
    }

    public function defensor()
    {
        return $this->belongsTo('App\Ejercito');
    }

    public function vencedor()
    {
        return $this->belongsTo('App\Ejercito');
    }
}
