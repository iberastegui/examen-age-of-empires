<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Unidad extends Model
{
    //este modelo inicialmente se
    //penso como una pivot table entre ejercito y tipo unidad,
    //y si bien podria extender de pivot model, preferi hacerlo
    //una entidad propia con su logica de entrenamiento y
    // trasnformacion(si lo hubiera hecho pivot model, el
    //entrenamiento y la trasnformacion lo hubiera puesto
    //en el modelo TipoUnidad y me hacia ruido que esa logica
    //estuviera ahi, solo por eso es una entdidad propia,
    // generalmente en estos casos si no tiene logica propia,
    //no lo hago modelo propio y solamente es una tabla intermedia
    // entre sus padres sin modelo propio, trato de evitar los pivot model en general)

    // tambien estaba entre nombrar fuerza_actual y cantidad_actual, fuerza y cantidad,
    // ya que estoy en unidad y no en tipo Unidad, pero preferi ser explicito.

    protected $table = 'unidades';
    protected $fillable = ['ejercito_id', 'tipo_unidad_id', 'fuerza_actual', 'cantidad_actual', 'puntos'];
    protected $appends = ['unidad_transformable'];

    /* no es necesario este accesor pero quize matar 2 pajaros de 1 tiro, este accesor me sirve
    tanto para saber si es transformable o no, como para saber escribiendo apenas menos a que se trasnforma.
    //*/

    public function getUnidadTransformableAttribute()
    {
        return $this->tipoUnidad->transformacion_id != null
        ? $this->tipoUnidad->transformacion_id
        : null;
    }

    public function tipoUnidad()
    {
        return $this->belongsTo('App\TipoUnidad');
    }

    public function ejercito()
    {
        return $this->belongsTo('App\Ejercito');
    }

    public function entrenar()
    {
        if ($this->ejercito->monedas_oro < $this->tipoUnidad->costo_entrenamiento) {
            return false;
        }

        DB::transaction(function () {
            $oroActual = $this->ejercito->monedas_oro - $this->tipoUnidad->costo_entrenamiento;
            $this->ejercito()->update(['monedas_oro' => $oroActual]);
            $this->fuerza_actual += $this->tipoUnidad->fuerza_por_entrenamiento;
            $this->puntos = $this->fuerza_actual * $this->cantidad_actual;
            $this->save();
            return true;
        });
        return false;

    }

    // aca asumo que cuando se transforma la unidad transformada queda en el nivel
    //base de la transformacion,perdiendos todo entrenamiento previo
    public function transformarse()
    {
        if ($this->ejercito->monedas_oro < $this->tipoUnidad->costo_entrenamiento ||
            $this->unidad_transformable == null) {
            return false;
        }

        DB::transaction(function () {
            $oroActual = $this->ejercito->monedas_oro - $this->tipoUnidad->costo_entrenamiento;
            $this->ejercito()->update(['monedas_oro' => $oroActual]);

            $nuevoTipoUnidad = TipoUnidad::find($this->unidad_transformable);

            $this->tipo_unidad_id = $nuevoTipoUnidad->id;
            $this->fuerza_actual = $nuevoTipoUnidad->fuerza_inicial;
            $this->puntos = $this->fuerza_actual * $this->cantidad_actual;

            $this->save();
            return true;
        });
        return false;

    }

}
